<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sakila</title>
</head>
<body>
    <h1>Lista de categorías</h1>
    <table class="table table-hover">
        <tr>
                <th>
                    Nombre 
                </th>
                <th>
                    Actualizar
                </th>
            </tr>
            @foreach ($categorias as $c)<tr>
                <td> {{ $c->name }} </td>
            <td><a href="{{ url('categorias/edit/'.$c->category_id )}}">
                    Actualizar
                    </a>
            <td>
            </tr>
            @endforeach
    </table>
 {{ $categorias->links() }}
</body>
</html>